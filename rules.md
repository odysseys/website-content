Time to begin your Odyssey.

To begin your odyssey you will first need to construct your Archive. Your archive is made up of 130 cards formed from your collection. Remember that from these cards you will need to be able to construct a 65 card Chronicle of up to 3 Elements.

Now that you have your Archive, you and your opponent take turns choosing your Elements for the game.
We suggest you flip a coin or roll a dice to decide who goes first.

For example, if your opponent selects Fire, you might then choose Earth, your opponent takes Metal, you select Wood, they pick Air, and you would be left with Water.

Now you should have a combination of 3 Elements and its time to use these Elements to make your Chronicle of 65 cards.
Remember you can only use cards from your Archive with the those same Elements that you picked earlier.

In our example above, you could make a deck only from your Earth, Wood, and Water cards.

Now that you and your opponent have made your Chronicles it's time to start the game.
The person who chose the first Element goes second.

You begin your turn by each drawing 7 cards from the top of your Chronicle this will be your hand (this would be your maximum hand size unless you have a card that says otherwise).
If you are the starting player you will not draw an additional card at the start of the game.

# Life and Death

When a player is dealt Damage, that many cards are taken from the top of the Chronical, and placed into their Deathzone.
If any player has 15 or more cards in their Deathzone, that player has instantly lost.

# Phases

I will now take you through the phases of the game :

1. Stand your cards
2. Draw cards
3. Main phase (combat, placing resources)
4. End phase
5. Cleanup

## Draw Phase

Draw a single card from the top of your Chronicle.
You may have 8 cards at this point, but that's okay, for now.

## Main Phase

### Casting

After your draw step you enter your main phase.
In this phase you can place up to 1 Resource card, unless you play a card that states otherwise.
You can Exhaust ('turn') your Resource cards to gain those Resources (turn an Air Resource in order to gain an Air Resource.

Your resources are then used to play cards from your hand.
Typically, Air cards will be require one or more Air Resources, and sometimes general Resources from any Element.

For example: 'Elvish Warrior', is a Wood card, which costs 1 Wood Resource and 2 other Resources from any Element.
For example, a player might Exhaust 1 Water Resource, 1 Earth Resource, and a Wood Resource, to play Elvish Warrior.

### Combat

You may have any one unit may attack at any point during the Main Phase.

To make a Unit attack, Exhaust it (i.e. turn the card to the right) and declare that it's attacking and choose what it is attacking, either an opponent directly, an Alchemist an opponent controls or a unit the player controls.

The opponent may then declare any of their own Units as 'blockers'.
If the attacking Unit is not blocked, then it deals damage equal to its Power the chosen target be that the opponent, unit or Alchemist.

If the unit is attacking a unit, only units with the blocker ability can be used to block.

If the attacking Unit *is* blocked, then all Units in that combat deal Damage equal to their total power.
If multiple Units are blocking, then the attacking Unit's player may assign damage to the units in any order, damage is dealt until the unit dies and all damage is dealt.

If any Unit receives Damage equal to or greater than its Defence, then that Unit is removed from the Battlefield and sent to the Graveyard.

## End Phase

The player can declare an End Phase at any point of the Main Phase.
This ends the turn.

## Cleanup

If you have more than 7 cards in your Hand, discard cards until you have 7.
Note any effects on the battlefield which state 'until end of turn'; these activate now.

# Card Types

The game has six types of cards: Conjured, Tricks, Units, Enhancements, Alchemist and Resources.

## Conjurations

Conjuration cards are powerful and flexible, with the ability to do almost anything in principle.
However, they are slow, cannot be cast on an opponent's turn, and cannot be quickly played in response to events on the Battlefield.

## Tricks

Tricks can be played at any point, even to interrupt a spell or Unit ability on an opponent's turn.

## Units

Units remain on the Warzone area (see below), and have both a Power and Defence stat.
These are your main way to damage your opponent, and defend against an opponent's Units.

## Enhancements

Enhancements grant abilities.
Sometimes they might stand alone in the Enhancements area, and grant repeated abilities, similar to Conjuration cards.

Other Enhancements modify Units, perhaps granting a boost to Damage and Defence, or granting the Unit some ability.

## Resources

Any card may be placed as a Resource, but 'Resource cards' can *only* ever be played in the Resources area.
These typically grant greater Resources than non-Resource cards.

# Locations

There are 7 areas in the game.
You don't need to draw lines on your table, but the cards should clearly be placed together.

## Resources

Regular cards can be placed in the Resources area to show that they only grant a Resource, such as 'Air', or 'Earth'.

## Enhancements

All global Enhancements remain in the Enhancements area.

## Warzone(are we happy with this term over Battlefield)

This is where Units remain, and including when they have been Exhausted to attack.

## Graveyard

Discarded cards, such as used Conjured and Tricks, or Units which have been destroyed, are sent to the Graveyard.
This will always be a pile of cards.

Any player may check the contents of any Graveyard at any point.

## Deathzone

When players receive Damage, that many cards are taken from their Chronicle, and placed into their Deathzone.
When a player has 15 or more cards in their Deathzone, they lose the game.

Deathzones effectively track life totals.

## Hand

The hand is a theoretical area, so you don't need to hold the cards in your actual hands at all times.
Cards in hand may not be shown to any other player.

## The Stack

The stack is another semi-theoretical area.
When a card is cast, it's placed into 'the stack', and if any Tricks, or other fast abilities are cast in response, each one goes onto the stack.

The last effect created is the first to resolve.

# Order of Play

## Picking Elements

One player goes first at random.

That player selects one of the six elements - Air, Earth, Fire, Metal, Water, or Wood.
The other player selects another Element, and both take turns, until each have 3 elements selected.

The players then take their entire 130 card Archive and use only their selected Elements to craft a Chronicle (i.e. the deck they will play with).

## Turn Order

The player who selected an Element first goes last.

### Draw Phase

The turn begins with players 'standing' their cards (so any previously Exhausted cards can be used again).
The player then draws a single card from their Chronicle, then starts the Main Phase.

### Main Phase

During the Main Phase, the player can play any card, such as Resources, Conjured cards, Tricks, or Enhancements.

During the Main Phase a Combat Phase can happen, this is a special phase and does not effect any players resource pools.

Only one Resource may be played each turn.

### End Phase

The player declares the end of their turn.

### Cleanup

Finally, the player cleans up by discarding down to 7 cards (if they had more) and taking care of any end of turn effects. Damage is removed from units.

## Stack

Fast effects, such as Tricks, or Unit abilities can interrupt each other.
In such cases, the first effect played is the last to take effect.
Players 'stack', the effects upon one another until there are no more.
At that point, the last effect played takes effect, and then both players simply work down the pile until all effects have resolved, in reverse order of play. Players can respond to effects on the stack as they are resolving while the stack is emptying.
