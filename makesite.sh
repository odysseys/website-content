#!/bin/bash

for p in *md; do
	n=$(basename -s ".md" "$p")
	pandoc --metadata title="$n" --standalone "$p" -o "$n".html
done

sed -i '4i\<link rel="stylesheet" href="https://latex.now.sh/style.css">\' *html
