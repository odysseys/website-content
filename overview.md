We are the Ludologists.

We're a group of 5 people, who make games.  The current production is a card game called Odysseys of Alchemy.

It has a number of features:

# Nice to Watch

Each player selects one of the six elements:

- Air
- Earth
- Fire
- Metal
- Water
- Wood

Once you choose an element, your opponent cannot select it.  As a result, two players cannot play the same decks against each other, or know which deck ("Chronicle") they plan to use ahead of time.

As a result, no tournament should become too predictable.

# Anti Death-Spiral

Every time a player receives damage, that many cards are placed into their Deathzone.  Once you hit 15 cards in the Deathzone, you're dead.

However, each card has an extra ability that can be played from the Deathzone.

A card which costs 2 Fire to deal 4 Damage to an opponent's Unit might have the Deathzone ability 'Pay 3: Deal 1 Damage to an opponent'.

# Open Production

The cards and all source files to make the cards have been placed under a Creative Commons, Share-Alike, Non-Commercial licence.  Anyone who has card ideas can add them, and anyone else can use those cards.

If new card ideas become popular, they can be added to the collection.

You can find the project here:

[Git Link](https://gitlab.com/odysseys)

[Vassal Download](share/oddysseys_vassal)

[Quickstart Rules](Rules.md)

# Guaranteed Costings

Instead of 'eyeballing' the cost of a card, a computer algorithm decides on the proper cost of each card.  Costings will not necessarily be 'correct', but they will definitely be fair.

It also means that if costings are not correct, they can be changed.  For example, if direct damage is over-costed to the point of being mostly useless, changing the formula will not only fix future cards, but will fix all cards in the existing decks.

































```
                   kdc:.                                                    
                  ockONd0.                                                  
                  KckKNl0.                                                  
                   l:c:, ;'                                                 
                     .,:O0Nc                                                
                     .klo0Kk.                                               
                      .xxkxNX.                                              
                       .llOxkO.                                             
                         l:OX00'                                            
                          ldc00K.    ...                                    
                           ,o.,,.,cdkNXO0K0x                                
                            'dkKNKKl.                                       
                         .:ookXOlc                                          
                      'okxldo' lkX0'.                                       
                     .XKdc     KlkOKMk.                                     
                     xN.       kdOdlMMM'                                    
                    .d,        :lxxxxMMW.                                   
                                'dKOKlWMW.                                  
                                 'dK0KdWMWc                                 
                                  .dOKNKWMMc                                
                                   .xW0WxWMMc                               
                                    .dKKWxXMMo                              
                                     .xOON0KMMd                             
                                      .dO0MOOMMx                            
                                        oOOMOXMMx                           
                                         cxOWOKMMO.                         
                                          ;kxKkkMMX.                        
                                           :xxXk0MMN.                       
                                            ;xOXxxMMk                       
                                             'dx0xoMM                       
                                              .ll0dcW0                      
                                                .okolN,                     
                                                   'd:k                     
                                                     .'.                    
                                                                            
                                                                            
                                                                            
                                                                            
```
