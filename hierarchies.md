<style>
</style>

# Hierarchies

## Pari – Council of Elders  

The Pari have a council in which the eldest of their kin
survey and look after their lands. Before they reach their fifth hundredth birthday, before they become ‘One
With The Wind’, they seek out new members to replace their position on the
Council. Typically, those chosen have
reached their four-hundredth flight cycle. Though if a Pari is wise beyond their years, in very rare circumstances,
a chosen replacement could be younger as the Elder’s word is sacrosanct.

## Meer – Imperial Monarchy

An empire of the Sea and waters, by ancestral divinity, the
Meer empire has survived under the Ocean for Centuries. Its strength comes from the crashing waves and
monsters of the deep that they have deal with constantly. Their speed within the water is unparalleled and
their soldiers are trained extensively in the use of the water and its currents. While they do have their daily lives, the
Meer are very regimented in that everyone has their duty and their place within
the Empire. Other Meer do exist in
farther lands but they are not part of this concentrated Empire, this is one of
its kind. They inscribe upon tablets,
through stories and pictures, learning from their Ancestors is a big part for
the Meer and so the lessons of years ago are still fresh in the minds of many
today.

## Talpa – Familial (Matriarch/Patriarch)

They are a farming community with tunnels and burrows that
are vast and stretch for miles. Each
Talpa family has the eldest who looks after their family Unit and meetings will
be called whenever it is needed. The
head of each family will go into the what they call the ‘Chamber of Echoes’ to
decide on the affairs that are presented. The Talpa are also no strangers to travelling as their underground
networks will connect with other Talpa Communities as all their families are intertwined
in, thus they all in some way know each another.

## Ethala – Scattered

A very scarce race who are rumoured to have lived for as long
as the World has been alive, they chronicle the history of the World, visiting
other races. Taking stories and passing
them on, making sure that as much of the World is not only heard but more
importantly is remembered. Every now and
then an Ethala will meet another to mate and create offspring so it too can grow,
learn and remember. They are welcomed by
most races in the open and are seen as sightings of good luck and fortune,
however they are not welcomed by the Religious Order of the humans.  

The Ethala have never met together in full or in groups, but there are legends
or whispers of a tale of it once happening, and they say the World was changed
on that day.

## Fae – Elected Council

Every Forest area has an elected Council in which all
sentient creatures have a vote on who will be in their charge. The Fey are vast and different even within
their own race, it would be more practical to say the Fey aren’t even a race at
all, but a joint group made up of factions of other races of the Forest. Each subset of species has their own elected
member of the Forest to speak on their behalf. They do this by the course of one day, they take it in turns to place
their hands on the Great Tree and allow it to bloom a single flower upon its
branches. Each representative will have
a flower which is their symbol, and the flower that blooms the most, denotes
who is elected to the Council.

If a creature of the Fey was ever to move from one forest to
another, they must then abide by those Council’s tenets, as the rule of the
Forest is the Council’s law. So those
travelling must be aware, as one Forest’s rules may differ vastly from another,
such is the nature of the Fey, differing and changing, no two ever the same not
even within the span of a second.

## Humans & Machines – Religious Order

Machine has made itself into the living God of
the humans, thus the humans are compelled to do its bidding. It has brought them wealth and comfort,
though they do not know that slowly they are being manipulated and enslaved to
its will. Most humans are ignorant of
this fact and just live to get by in their day to day lives, whereas the most fervent
and zealot of the humans have given themselves over to the Machine and created
a Religious Order which is at the heart of human society. This Order sets out the law of the human
lands and also sentences those who it deems traitorous to either exile or in
most cases, death. Some are a part of
this Order to be a part of something, others are there to bring about change in
human society, most are there because it’s what they grew up knowing to be the
right laws and then there are the powerful few, who are there for the glory and
control it gives them. Blissfully
unaware that the Machine has already taken them.  

Due to the adaptability of humans, there are those who have left their own
lands and Cities and co-exist in other races with other races in their lands.
